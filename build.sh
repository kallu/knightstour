#!/bin/sh

WIDGET="Knight's Tour"

CODE="Info.plist knight.html knight.css knight.js GoodbyeWorld.js"
IMAGES="Default.png Icon.png img1.png img2.png img3.png img4.png knight.png knight_invert.png license.png"

BUILD="${WIDGET}.wdgt"

mkdir "${BUILD}"
for F in ${CODE}
do
	cp "${F}" "${BUILD}/${F}"
done

for F in ${IMAGES}
do
	cp "images/${F}" "${BUILD}/${F}"
done

