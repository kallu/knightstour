//
// Knight's Tour
// Chess puzzle widget for MacOS X Dashboard
//
// - Petri Kallberg Mar/08/2008
//
// This work is licensed under the Creative Commons Attribution-Noncommercial-Share Alike 1.0 Finland License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/1.0/fi/
// or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.
// 

var myPosition;
var myMove;

function printScore(score, max) {
	document.getElementById("score").innerHTML = "Score " + score + "/" + max;
}

function moveKnight(event) {

	var i = event.target;
	if (isValidMove(myPosition, i)) {
		if (myPosition) {
			myPosition.className="white";
			showAllValidMoves(myPosition, "black");
		}
		myPosition = document.getElementById(i.id);
		myPosition.className="knight";
		myMove = myMove + 1;
		showAllValidMoves(myPosition, "valid");
		printScore(myMove, 64);
	}
}

function isValidMove(from, to) {

	if (from == null) { return(true); }
	
	var p1 = String(from.id), x1 = Number(p1.charAt(3)), y1 = Number(p1.charAt(2));
	var p2 = String(to.id), x2 = Number(p2.charAt(3)), y2 = Number(p2.charAt(2));
	
	return(isLegalMove(x1, y1, x2, y2));
}

function showAllValidMoves(from, style) {

	var p = String(from.id), x = Number(p.charAt(3)), y = Number(p.charAt(2));

	if (isLegalMove(x, y, x+2, y+1)) { setSquare(x+2,y+1,style); }
	if (isLegalMove(x, y, x+2, y-1)) { setSquare(x+2,y-1,style); }
	if (isLegalMove(x, y, x-2, y+1)) { setSquare(x-2,y+1,style); }
	if (isLegalMove(x, y, x-2, y-1)) { setSquare(x-2,y-1,style); }
	if (isLegalMove(x, y, x+1, y+2)) { setSquare(x+1,y+2,style); }
	if (isLegalMove(x, y, x+1, y-2)) { setSquare(x+1,y-2,style); }
	if (isLegalMove(x, y, x-1, y+2)) { setSquare(x-1,y+2,style); }
	if (isLegalMove(x, y, x-1, y-2)) { setSquare(x-1,y-2,style); }
}

function setSquare(x, y, style) {
	(document.getElementById("sq" + y + x)).className=style;
}

function isLegalMove(x1, y1, x2, y2) {
	
	if (x1 < 1 || x2 < 1 || y1 < 1 || y2 < 1) { return(false); }
	if (x1 > 8 || x2 > 8 || y1 > 8 || y2 > 8) { return(false); }
	if ((document.getElementById("sq" + y2 + x2)).className == "white") { return(false); }
	
	if (x1+1 == x2 && y1-2 == y2) { return(true); }
	if (x1-1 == x2 && y1-2 == y2) { return(true); }
	if (x1+2 == x2 && y1+1 == y2) { return(true); }
	if (x1+2 == x2 && y1-1 == y2) { return(true); }
	if (x1+1 == x2 && y1+2 == y2) { return(true); }
	if (x1-1 == x2 && y1+2 == y2) { return(true); }
	if (x1-2 == x2 && y1+1 == y2) { return(true); }
	if (x1-2 == x2 && y1-1 == y2) { return(true); }
	
	return(false);
}

// The javascript Math.random() method is really bad. I use this
// instead. It's a "linear congruential generator" right out of
// "Numerical Recipes". It's not cryptographically sound, but it's
// fine for dice-rollers or roulette-wheel-simulators.
//
// For random number between 0-x: Math.round(random()*x);
//           -- Joshua Emmons (skia.net) 
//           -- Nov/16/05
//
// This license intentionally left blank. COPY IT! STEAL IT! MAKE IT YOUR OWN!

random.m=714025; random.a=1366; random.c=150889;
random.seed = (new Date()).getTime()%random.m;
function random() {
  random.seed = (random.seed * random.a + random.c) % random.m;
  return random.seed / random.m;
}


function init() {

	var x = Math.floor(1+(random()*8));
	var y = Math.floor(1+(random()*8));
	
	myMove = 1;
	myPosition=document.getElementById("sq" + y + x);
	
	setSquare(x, y, "knight");
	showAllValidMoves(myPosition, "valid");
	printScore(myMove, 64);
	
	var doneButton = document.getElementById("doneButton");
	createGenericButton(doneButton, "Done", hidePrefs);
}

